const {app, BrowserWindow} = require('electron');

let root;

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (root === null) {
        createWindow();
    }
});

app.on('will-quit', function () {
    globalShortcut.unregisterAll()
})

function createWindow(){
    root = new BrowserWindow({
        width: 1280,
        height: 720,
        resizable: false,
        maximized: false,
        show: false
    });
    root.loadURL("file://" + __dirname + "/game/index.html");
    root.setMenu(null);
    root.show();
}